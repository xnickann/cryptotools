from telebot import TeleBot, types, apihelper
# pip install pyTelegramBotAPI
import ccxt
# pip install ccxt
from datetime import datetime, timedelta
from random import shuffle
from apscheduler.scheduler import Scheduler
# pip install apscheduler==2.1.2
from time import sleep
import sqlite3, operator
import gc

# UBUNTU
# sudo apt-get install python-pip python-dev libffi-dev libssl-dev libxml2-dev libxslt1-dev libjpeg8-dev zlib1g-dev

# Telegram Bot Token
TOKEN = 'token'
# Telegram Canal Name
tg_canal='@cpb_test'

# if you need run telegram bot with proxy
# apihelper.proxy = {'https': 'https://59.127.168.43:3128'}

ignore_min = 5  # how many minutes to throw the market to ignore after notification
limit = 6  # minimum price change for notification (percent)
startminutes = 5  # how many minutes is necessary to collect data after running the script (recommended 5)
f = open('auto_excluded_markets.txt', 'w')
f.write('')
f.close()

bot = TeleBot(TOKEN)
bittrex = ccxt.bittrex()
conn = sqlite3.connect('db.db')
cursor = conn.cursor()
starttime = datetime.now()
sched = Scheduler()
sched.start()

def rd(x, y=0):
    ''' A classical mathematical rounding by Voznica '''
    m = int('1' + '0' * y)  # multiplier - how many positions to the right
    q = x * m  # shift to the right by multiplier
    c = int(q)  # new number
    i = int((q - c) * 10)  # indicator number on the right
    if i >= 5:
        c += 1
    return c / m

def get_ignore_list():
    f = open('auto_excluded_markets.txt')
    marketlist = [line.replace('\n', '') for line in f]
    f.close()
    return marketlist

def get_blacklist():
    f = open('blacklist.txt')
    blacklist = [line.replace('\n', '') for line in f]
    f.close()
    return blacklist

def remove_from_ignore(marketname):
    try:
        marketlist = get_ignore_list()
        newlist = []
        i = 0
        while i < len(marketlist):
            if marketlist[i] != marketname and marketlist[i] != '\n':
                newlist.append(marketlist[i])
                del marketlist[i]
            else:
                i += 1
        f = open('auto_excluded_markets.txt', 'w')
        for line in newlist:
            f.write(line + '\n')
        print('{} has been deleted from ignore'.format(marketname))
    except Exception as e:
        print(datetime.now(), '-', e)


def check_market(marketname):
    result = '<a href="{url}">{name}</a>'.format(url = 'https://international.bittrex.com/Market/Index?MarketName=' + marketname.replace('_','-'),
                                               name = marketname[4:]) + ' '
    _result = marketname[4:] + ''
    need_send = False
    periods = [300, 60, 10]
    newcursor = conn.cursor()
    for x in periods:
        sqlrow = "SELECT * FROM {} WHERE CAST(strftime('%s', checked)  AS  integer) >= CAST(strftime('%s', \'{}\') AS  " \
                 "integer) ORDER BY checked DESC;".format(marketname, datetime.now() - timedelta(seconds=x+3))
        newcursor.execute(
            sqlrow)
        rows = [x[0] for x in newcursor.fetchall()]
        if len(rows) > 1:
            min_index, min_value = min(enumerate(rows), key=operator.itemgetter(1))
            max_index, max_value = max(enumerate(rows), key=operator.itemgetter(1))
            multiple = 1 if max_index < min_index else -1
            if min_value > 0 and max_value!=min_value:
                percent = int(rd(((max_value - min_value) / min_value) * 100)) * multiple
            else:
                percent = 0
            summ=0
            if abs(percent) >= limit: 
                need_send = True
                try:
                    history=bittrex.public_get_markethistory(params={'market':marketname.replace('_','-')}).get('result')
                    back=datetime.utcnow()-timedelta(seconds=x)
                    for h in history:
                        ts=h.get('TimeStamp')[:h.get('TimeStamp').find('.')]
                        if datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S') > back:
                            summ+=h.get('Total')
                        else:
                            break
                except Exception as e:
                    print(e)
            summ='[V:'+str(round(summ))+']'
            timeline = '{}m'.format(x // 60) if x > 59 else '{}s'.format(x)
            if x<300:
                result += '( {}%/{})'.format(percent, timeline)
            if x>=300:
                result += '( {}%/{}{})'.format(percent, timeline, summ)
                _result += '( {}%/{})'.format(percent, timeline)
    if need_send and datetime.now() > starttime + timedelta(seconds=startminutes * 60):
        if _result not in get_ignore_list() and marketname not in get_blacklist():
            try:
                f = open('auto_excluded_markets.txt', 'a')
                # f.write(marketname + '\n')
                f.write(_result + '\n')
                f.close()
                now = datetime.now()
                run_at = now + timedelta(seconds=ignore_min * 60)
                sched.add_date_job(remove_from_ignore, run_at, [_result])
                print(datetime.now(),'-',result)
                bot.send_message(
                    chat_id=tg_canal,
                    text=result,
                    parse_mode="HTML",
                    disable_web_page_preview=True)
            except Exception as e:
                print(datetime.now(), '-', e)

while (True):
    try:
        markets = [x for x in bittrex.public_get_marketsummaries()['result'] if x['MarketName'].startswith('BTC')]
        cursor.execute("select * from sqlite_master where type = 'table'")
        tablelist = [x[1] for x in cursor.fetchall()]
        for market in markets:
            marketname = market['MarketName'].replace('-', '_')
            if marketname not in tablelist:
                cursor.execute("""CREATE TABLE {}
                (price FLOAT(8, 8),
                checked DATE);""".format(marketname))
                print('new market:', marketname)
            cursor.execute(
                "DELETE FROM {} WHERE CAST(strftime('%s', checked)  AS  integer) < CAST(strftime('%s', \'{}\')  "
                "AS  integer);".format(marketname, datetime.now() - timedelta(seconds=300)))
            conn.commit()
            sqlrow = "INSERT INTO {} (price, checked) VALUES ({}, \'{}\')".format(marketname, f"{market['Last']:.8f}", datetime.now())
            cursor.execute(sqlrow)
            conn.commit()
            check_market(marketname)
        gc.collect()
        print(datetime.now(), '- markets checked')
        del markets
    except Exception as e:
        print(datetime.now(), '-', e)