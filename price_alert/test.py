import ccxt
from datetime import datetime, timedelta

b=ccxt.bittrex()

def fix(x):
	if x.endswith('BTC'):
		x=x.replace('/BTC','')
		x='BTC-'+x
		return x
	else:
		return None

markets=[i for i in list(map(lambda x: fix(x), b.load_markets(True).keys())) if i]
print(len(markets))
for marketname in markets:
	history=b.public_get_markethistory(params={'market':marketname}).get('result')
	back=datetime.utcnow()-timedelta(seconds=300)
	summ=0
	for h in history:
		ts=h.get('TimeStamp')[:h.get('TimeStamp').find('.')]
		if datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S') > back:
			summ+=h.get('Total')
		else:
			break
	print(marketname,'-','[V:{}]'.format(round(summ)))