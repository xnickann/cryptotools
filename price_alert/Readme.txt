1. Для начала необходимо создать Telegram Bot'a (@BotFather) и сохранить сгенерированный токен.
2. Создать Telegram канал и добавить в администраторы созданного бота.
3. С помощью текстового редактора открыть файл pricealertbot.py и присвоить переменной TOKEN (14 строка) токен от своего Telegram бота; переменной tg_canal (16 строка) - ссылку на Telegram канал.
4. Для размещения бота рекомендуется использовать VDS сервер с минимальными характеристиками: 1 CPU, 1Gb RAM, скорость проверки всех маркетов зависит от мощности сервера. 
5. Для работы скрипта на сервере необходимо установить Python 3.6+ (tutorial for ubuntu 16+ - http://ubuntuhandbook.org/index.php/2017/07/install-python-3-6-1-in-ubuntu-16-04-lts/) и с помощью менеджера пакетов pip установить следующие библиотеки:
- pip install apscheduler==2.1.2
- pip install ccxt
- pip install pyTelegramBotAPI
6. Для удаленного запуска скрипта можно использовать команду следующего вида:
ssh root@192.168.1.1 "python3 /path/to/your/script/script_coinpulse_bittrex.py > /dev/null &"

P.S.:
- Процент изменения цены на который реагирует бот можно изменить в строчке 22
- В случае появления новых маркетов - они автоматически добавляются в базу скрипта. 
- Периоды (в секундах) за которые необходимо выводить статистику можно изменить в строке 72 (при необходимости выводить информацию за период больший, чем 5 минут, необходимо внести соответствующие изменения в строку 122)
- По умолчанию после запуска скрипту необходимо 5 минут на сбор актуальной информации.

Описание файлов:
auto_excluded_markets.txt - рабочий файл, в него дублируются сообщения в канале, для того чтобы исключить повторы сообщений

blacklist.txt - файл в который пользователь самостоятельно вносит валютные пары, которые он хотел бы исключить из выдачи(по умолчанию опрашиваются все пары к BTC)

db.db - рабочий файл, локальная база в которую записываются все целевые показатели за нужный отрезок времени

script_coinpulse_bittrex.py - скрипт на питоне. для передачи сигналов в канал ТГ, нужно вместо знака ##### вставить ключ(токен), выданный botfather’ом в ТГ

start - bash скрипт для быстрого запуска

1. Firstly you need to create a Telegram Bot (@BotFather) and save generated token.
2. Then to create a Telegram channel and add the created bot to the administrators.
3. Using a text editor, open the script_coinpulse_bittrex.py file and assign the TOKEN variable (14 lines) token from your Telegram bot; variable tg_canal (16 line) - link to the Telegram channel.
4. For deploy it`s recommended to use a VDS server with the minimum values: 1 CPU, 1 GB of RAM, the speed of checking all markets depends on the server's capacity.
 
5. To run the script on the server you need to install Python 3.6+ (tutorial for ubuntu 16+ - http://ubuntuhandbook.org/index.php/2017/07/install-python-3-6-1-in-ubuntu-16-04-lts/) and using the pip package manager install the following libraries:
- pip install apscheduler == 2.1.2
- pip install ccxt
- pip install pyTelegramBotAPI
6. To run the script remotely, you can use the following command:
ssh root@192.168.1.1 "python3 /path/to/your/script/script_coinpulse_bittrex.py> / dev / null &"
 
P.S .:
- The percentage of price changes that the bot responds can be changed in the line 22
- In case of new markers - all of them are automatically added to the script database.
- Periodically (in seconds) it`s necessary to change the statistics 72 (when it`s neccesary to display information in the period that is longer than 5 minutes, you need to make changes to the results 122)
- By default, after launching, the script needs 5 minutes to collect relevant information. 

Description of files:
auto_excluded_markets.txt - a working file in which the messages in the channel are duplicated, in order to avoid repetition of messages (ред.)
 
blacklist.txt - the file in which the user independently enters currency pairs, which he would like to exclude from the issue (by default, all pairs are polled to BTC).

db.db - file, local database in which all target indicators are recorded for the required period of time.

script_coinpulse_bittrex.py is a python script. To transmit signals to the TG channel, instead of the ##### sign, insert the key (token) issued by the botfather in the TG

start - bash script for quick launch.