import csv
from six.moves import cPickle as pickle
from datetime import datetime

def main(path_csv, path_pickle):
    start = datetime.now()
    print('Начинаем перевод...')
    x = set()
    with open(path_csv,'r') as f:
        reader = csv.reader(f)
        for line in reader: x.add(line[0])

    with open(path_pickle,'wb') as f:
        pickle.dump(x, f, pickle.HIGHEST_PROTOCOL)
    end = datetime.now()
    total = end - start
    print('Готово! Время выполнения - {}'.format(total))
main('base.csv', 'base.pickle')
