from multiprocessing import Pool
import csv
from datetime import datetime
import requests
from bs4 import BeautifulSoup

filename='filename.txt'

def get_html(url):
    response = requests.get(url)
    return response.text

def get_all_adresses(html):
    soup = BeautifulSoup(html, 'lxml')
    trs=[]
    try:
        trs = soup.find('table', class_='table table-hover').find('tbody').find_all('tr')
    except:
        return []
    rows = []
    for tr in trs:
        tds=tr.find_all('td')
        address = tds[0].find('a').text
        hashs = tds[1].find('a').text
        btcvalue=tds[2].text.split('\xa0BTC')[0]
        try:
            if float(btcvalue)>0.01:
                rows.append({'address': address, 'hash' : hashs})
        except:
            rows.append({'address': address, 'hash' : hashs})
    return rows

def main():
    print('Парсим адреса и хеши до 31/11/2012....')
    threads=int(input('Введите количество потоков (рекомендуется 10-20) - '))
    global filename
    filename='HASHes-31-11-2012.txt'
    start = datetime.now()
    all_links=['https://bitcoinchain.com/block_explorer/catalog/{}'.format(x) for x in range(1, 80451)]
    with Pool(threads) as p:
        p.map(make_all, all_links)
    end = datetime.now()
    total = end - start
    print('=================')
    print('total time: ' + str(total))

def csv_writer(data):
	global filename
	with open(filename, 'a') as fp:
		writer = csv.writer(fp, delimiter=',')
		writer.writerows(data)

def write_txt(data):
    with open(filename, 'a') as f:
        for x in data:
            f.write(x.get('address') + ' ' + x.get('hash') + '\n')
        
def make_all(link):
    start = datetime.now()
    html = get_html(link)
    data = get_all_adresses(html)
    if(len(data)!=0):
        #csv_writer(data)
        write_txt(data)
        end = datetime.now()
        total = end - start
        print('{} - checked. {} hashes found ({} sec)'.format(link ,len(data),total))
    else:
        print('{} - checked. Hashes not found'.format(link))
    
if __name__ == '__main__':
     main()