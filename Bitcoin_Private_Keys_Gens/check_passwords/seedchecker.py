# -*- coding: utf-8 -*-
from bitcoin import *
import ssl
import multiprocessing
from multiprocessing import Pool, Manager, Value 
from pywallet import wallet

pass_list = 'passwords.txt'
ssl._create_default_https_context = ssl._create_unverified_context

def check_process():
    while True:
        try:
            seed = wallet.generate_mnemonic()
            w = wallet.create_wallet(network="BTC", seed=seed)
            priv = w.get('wif').decode("utf-8")
            addr = w.get('address')
            h = history(addr)
            print('Адрес: {} | С историей: {} | С балансом: {} | Password: {}'.format(addr, count_passwords('with_history.txt'), count_passwords('with_balance.txt'), seed))
            if h and any(h):
                print('Найдена история для пароля {} по адресу: {}'.format(seed, addr))
                with open('with_history.txt', 'a') as f:
                    f.write('Address: {} | WIF Key:{} | Password: {}\n'.format(addr, priv, seed))
                u = unspent(addr)
                if u and any(u):
                    with open('with_balance.txt', 'a') as f:
                        f.write('Address: {} | WIF Key:{} | Password: {}\n'.format(addr, priv, seed))
                    print('Найден баланс на кошельке:')
                    print('Address: {}'.format(addr))
                    print('WIF Key: {}'.format(priv))
            priv=None
            pub=None
            addr=None
            h=None
            percent=None
        except Exception as e:
            print('Ошибка на пароле: {}'.format(seed))
            print(e)
            
def count_passwords(filename):
    t = 0
    with open(filename, encoding='latin-1') as fp:
        for line in fp:
            t += 1
    return t

def check():
    print('======================')
    print('Запускаем чекер...')
    print('======================')
    for cpu in range(multiprocessing.cpu_count()):
        multiprocessing.Process(target = check_process).start()
    return

if __name__ == '__main__':
    check()
