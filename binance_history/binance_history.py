import ccxt
from datetime import datetime, timedelta
import requests
from time import sleep


# d1=datetime.fromtimestamp(1516318237)
# d2=datetime.utcnow()
# print(abs((d2 - d1).days))
# sleep(100)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

days=2000
hours=24*days
effort_percent_btc=0
effort_percent_usdt=0
effort_btc=0
effort_usdt=0

first_day_btc=0
first_day_usdt=0

deposize=0
wdsize=0
pribel=0

exchange_id = 'binance'
exchange_class = getattr(ccxt, exchange_id)
exchange = exchange_class({
    'apiKey': 'apikey',
    'secret': 'secretkey',
    'timeout': 30000,
    'enableRateLimit': True,
})

def get_history_price(start_time, symbol):
	try:
		counter=0
		pricelist=exchange.public_get_klines(params={'symbol':symbol, 'interval':'1m', 'startTime':start_time, 'limit':1})
		while len(pricelist)==0:
			counter+=1
			if counter==10:
				#return exchange.public_get_ticker_price(params={'symbol' : symbol}).get('price')
				return 0
			start_time=int((datetime.utcnow()-timedelta(hours=24*counter)).timestamp())*1000
			sleep(0.2)
			pricelist=exchange.public_get_klines(params={'symbol':symbol, 'interval':'1m', 'startTime':start_time, 'limit':1})
		price=float(pricelist[0][4])
		# print(datetime.fromtimestamp(start_time).strftime('%y-%m-%d'), symbol, price, 'BTC')
		#print(start_time, symbol, price, 'BTC')
	except Exception as e:
		print(e)
		return -1
	return price

def get_btc_price():
	url='https://blockchain.info/ticker'
	r=requests.get(url)
	return float(r.json().get('USD').get('last'))

def get_usdt_price(start_time):
	# url='https://blockchain.info/ticker'
	# r=requests.get(url)
	# return float(r.json().get('USD').get('last'))
	return get_history_price(start_time, 'BTCUSDT')

yesterday=int((datetime.utcnow()-timedelta(hours=hours)).timestamp())*1000

btcprice=get_btc_price()

def get_balance_per_day(balances, start_time, end_time, deposits, withdrawals, orders, btcsum, usdtsum):
	global effort_percent_btc
	global effort_percent_usdt
	global effort_btc
	global effort_usdt

	global first_day_btc
	global first_day_usdt
	global deposize
	global wdsize

	ef=open('results/effort.txt', "a")

	count_orders=0
	bal_list=[]
	today_deposits={}
	today_withdrawals={}
	btcprice=get_usdt_price(start_time)
	f=open('results/per_day/{}.txt'.format(datetime.fromtimestamp(start_time//1000).strftime('%Y-%m-%d')),"w")
	fa=open('results/total.txt', "a")
	f.write('{}\n=====================\n'.format(datetime.fromtimestamp(start_time//1000).strftime('%d-%m-%y %H:%M:%S')))
	for dep in deposits:
		if start_time>=dep.get('timestamp')>end_time:
			if(dep.get('status')!='ok'):
				print('deposit failed')
				f.write('deposit failed\n')
				continue
			print('new deposit: {} / {} {}'.format(
				datetime.fromtimestamp(dep.get('timestamp')//1000).strftime('%d-%m-%y %H:%M:%S'), 
				dep.get('amount'), dep.get('currency')))
			f.write('new deposit: {} / {} {}\n'.format(
				datetime.fromtimestamp(dep.get('timestamp')//1000).strftime('%d-%m-%y %H:%M:%S'), 
				dep.get('amount'), dep.get('currency')))
			if dep.get('currency') in balances:
				balances[dep.get('currency')]+=dep.get('amount')
			else:
				balances[dep.get('currency')]=dep.get('amount')
			if dep.get('currency') in today_deposits:
				today_deposits[dep.get('currency')][0]+=dep.get('amount')
			else:
				today_deposits[dep.get('currency')]=[dep.get('amount'),0, 0]
	for order in orders:
		if start_time>=order.get('timestamp')>end_time:
			print('new order:' ,
				datetime.fromtimestamp(order.get('timestamp')//1000).strftime('%d-%m-%y %H:%M:%S'), 
				order.get('amount'), order.get('av'), '/', order.get('cost'), order.get('cv'), '/ commission:', order.get('commission'), order.get('comv'))
			f.write('new order: {} / {} {} / {} {} / commission {} {}\n'.format(
				datetime.fromtimestamp(order.get('timestamp')//1000).strftime('%d-%m-%y %H:%M:%S'), 
				order.get('amount'), 
				order.get('av'), 
				order.get('cost'), 
				order.get('cv'), 
				order.get('commission'), 
				order.get('comv')))
			if order.get('av') in balances:
				balances[order.get('av')]+=order.get('amount')
			else:
				balances[order.get('av')]=order.get('amount')
			if order.get('cv') in balances:
				balances[order.get('cv')]+=order.get('cost')
			else:
				balances[order.get('cv')]=order.get('cost')
			# if order.get('comv')=='BNB':
			# 	continue
			if order.get('comv') in balances:
				balances[order.get('comv')]-=order.get('commission')
			else:
				balances[order.get('comv')]=(-1)*order.get('commission')
			count_orders+=1
	for wd in withdrawals:
		if start_time>=wd.get('timestamp')>end_time:
			if(wd.get('status')!='ok'):
				print('withdrawal failed')
				f.write('withdrawal failed\n')
				continue
			print('new withdrawal:', 
				datetime.fromtimestamp(wd.get('timestamp')//1000).strftime('%d-%m-%y %H:%M:%S'), 
				wd.get('amount'), wd.get('currency'))
			f.write('new withdrawal: {} / {} {}\n'.format(
				datetime.fromtimestamp(wd.get('timestamp')//1000).strftime('%d-%m-%y %H:%M:%S'), 
				wd.get('amount'), wd.get('currency')))
			if wd.get('currency') in balances:
				balances[wd.get('currency')]-=wd.get('amount')
			if wd.get('currency') in today_withdrawals:
				today_withdrawals[wd.get('currency')][0]+=wd.get('amount')
			else:
				today_withdrawals[wd.get('currency')]=[wd.get('amount'),0, 0]
	f.write('=============\nBALANCES\n')
	copy_balances=balances.copy()
	for b in copy_balances.keys():
		if b!='BTC':
			lastprice=0
			if b=='USDT':
				price=get_usdt_price(start_time)
				flprice=float(balances.get(b))/price
				bal_list.append(flprice)
				print('{coin:6} ({count}) - {btc} BTC / {usd} USD'.format(
					coin=b,
					count='%.8f' % float(balances.get(b)),
					btc='%.8f' % flprice,
					usd='%.3f' % (flprice*btcprice)))
				f.write('{coin:6} ({count}) - {btc} BTC / {usd} USD\n'.format(
					coin=b,
					count='%.8f' % float(balances.get(b)),
					btc='%.8f' % flprice,
					usd='%.3f' % (flprice*btcprice)))
				if b in today_deposits:
					today_deposits[b][1]+=today_deposits[b][0]/price # сумма депозита по монете в BTC
					today_deposits[b][2]+=(today_deposits[b][0]/price)*btcprice # сумма депозита по монете в USDT
				if b in today_withdrawals:
					today_withdrawals[b][1]+=today_withdrawals[b][0]/price # сумма вывода по монете в BTC
					today_withdrawals[b][2]+=(today_withdrawals[b][0]/price)*btcprice # сумма вывода по монете в USDT
				continue
			else:
				price=get_history_price(start_time, b+'BTC')
			if price!=-1:
				flprice=float(price)*float(balances.get(b))
				bal_list.append(flprice)
				if b in today_deposits:
					today_deposits[b][1]+=today_deposits[b][0]*price # сумма депозита по монете в BTC
					today_deposits[b][2]+=(today_deposits[b][0]*price)*btcprice # сумма депозита по монете в USDT
				if b in today_withdrawals:
					today_withdrawals[b][1]+=today_withdrawals[b][0]*price # сумма вывода по монете в BTC
					today_withdrawals[b][2]+=(today_withdrawals[b][0]*price)*btcprice # сумма вывода по монете в USDT
				if (flprice*btcprice)<=0.0008:
						balances.pop(b, None)
						print('Очистили остатки {}'.format(b))
						continue
				print('{coin:6} ({count}) - {btc} BTC / {usd} USD'.format(
					coin=b,
					count='%.8f' % float(balances.get(b)),
					btc='%.8f' % flprice,
					usd='%.3f' % (flprice*btcprice)))
				f.write('{coin:6} ({count}) - {btc} BTC / {usd} USD\n'.format(
					coin=b,
					count='%.8f' % float(balances.get(b)),
					btc='%.8f' % flprice,
					usd='%.3f' % (flprice*btcprice)))
			else:
				print(b)
		else:
			if b in today_deposits:
				today_deposits[b][1]+=today_deposits[b][0] # сумма депозита по монете в BTC
				today_deposits[b][2]+=(today_deposits[b][0]*btcprice)# сумма депозита по монете в USDT
			if b in today_withdrawals:
				today_withdrawals[b][1]+=today_withdrawals[b][0] # сумма вывода по монете в BTC
				today_withdrawals[b][2]+=(today_withdrawals[b][0]*btcprice) # сумма вывода по монете в USDT
			if float(balances.get(b))>0:
				usdprice=btcprice*float(balances.get(b))
				if (usdprice)<=0.0008:
						balances.pop(b, None)
						print('Очистили остатки {}'.format(b))
						continue
				print(b,'  (%.8f)' % float(balances.get(b)), '/',usdprice,'USD')
				f.write('{}    ({}) / {} USD\n'.format(
					b,
					'%.8f' % float(balances.get(b)),
					usdprice
					))
				bal_list.append(float(balances.get(b)))
				#print([x.get('lastPrice') for x in volumebook if x.get('symbol')=='BTC'+b])
	f.write('======================\nTOTAL: {} BTC | {} USD'.format(
		'%.8f' % float(sum(bal_list)),
		'%.3f' % float(sum(bal_list)*btcprice)))
	f.close()
	# fa.write('{} - {} BTC | {} USD\n'.format(
	# 	datetime.fromtimestamp(start_time//1000).strftime('%d-%m-%y %H:%M:%S'),
	# 	'%.8f' % float(sum(bal_list)),
	# 	'%.3f' % float(sum(bal_list)*btcprice)))
	sumbal=sum(bal_list)
	
	deposum_btc=0 if len(list(today_deposits.keys()))==0 else sum([x[1] for x in today_deposits.values()])
	wdsum_btc=0 if len(list(today_withdrawals.keys()))==0 else sum([x[1] for x in today_withdrawals.values()])
	deposum_usdt=0 if len(list(today_deposits.keys()))==0 else sum([x[2] for x in today_deposits.values()])
	wdsum_usdt=0 if len(list(today_withdrawals.keys()))==0 else sum([x[2] for x in today_withdrawals.values()])

	deposize+=deposum_usdt
	wdsize+=wdsum_usdt

	first_day_btc+=deposum_btc
	first_day_btc-=wdsum_btc
	first_day_usdt-=deposum_usdt
	first_day_usdt-=wdsum_usdt

	sumbal_btc_without_dw=(sumbal-deposum_btc)+wdsum_btc
	sumbal_usdt_without_dw=((sumbal*btcprice)-deposum_usdt)+wdsum_usdt

	if sumbal_btc_without_dw==0:
		sumbal_btc_without_dw=0.00000001
	if sumbal_usdt_without_dw==0:
		sumbal_usdt_without_dw=0.00000001
	if btcsum==0:
		btcsum=0.00000001
	if usdtsum==0:
		usdtsum=0.00000001

	plus_btc=sumbal_btc_without_dw-btcsum
	plus_usdt=sumbal_usdt_without_dw-usdtsum

	effort_percent_btc+=(plus_btc/sumbal_btc_without_dw) * 100
	effort_btc+=plus_btc
	effort_percent_usdt+=(plus_usdt/sumbal_usdt_without_dw) * 100
	effort_usdt+=plus_usdt

	fc=open('results/btc.txt', "a")

	ef.write('{} / BTC: {} ({}%) | USDT: {} ({}%)\n'.format(
		datetime.fromtimestamp(start_time//1000).strftime('%d-%m-%y %H:%M:%S'),
		'%.8f' % effort_btc,
		round(effort_percent_btc, 2),
		'%.3f' % effort_usdt,
		round(effort_percent_usdt, 2)
		))
	ef.write('{} / Депозиты: {}$ | Выводы: {}$ | Текущий баланс: {}$ | Заработал: {}$\n'.format(
		datetime.fromtimestamp(start_time//1000).strftime('%d-%m-%y %H:%M:%S'),
		'%.3f' % deposize,
		'%.3f' % wdsize,
		'%.3f' % (sumbal*btcprice),
		'%.3f' % ((wdsize+(sumbal*btcprice))-deposize)
		))
	ef.write('	{} Deposits; {} Withdrawals; {} Trades\n'.format(
		len(today_deposits.keys()),
		len(today_withdrawals.keys()),
		count_orders
		))
	ef.write('	было BTC: {} | стало (без учета выводов): {} | с учетом: {}\n'.format(
		'%.8f' % btcsum,
		'%.8f' % sumbal_btc_without_dw,
		'%.8f' % sumbal,
		))
	ef.write('	deposum: {}\n    wdsum: {}\n    sumbal_without_wd: {}\n    effort_btc: {}\n    plus_btc: {}\n\n'.format(
		'%.8f' % deposum_btc,
		'%.8f' % wdsum_btc,
		'%.8f' % sumbal_btc_without_dw,
		'%.8f' % effort_btc,
		'%.8f' % plus_btc
		))
	# ef.write('	было USDT: {} | стало (без вычета баланса): {} | с учетом: {}\n\n'.format(
	# 	'%.8f' % usdtsum,
	# 	'%.8f' % sumbal_usdt_without_dw,
	# 	'%.8f' % (sumbal*btcprice),
	# 	))
	fc.write('[new Date({}), {}, {}],\n'.format(
		start_time,
		'%.8f' % effort_btc,
		'%.8f' % plus_btc,
		))
	fc.close()

	fb=open('results/usdt.txt', "a")
	fb.write('[new Date({}), {}, {}],\n'.format(
		start_time,
		'%.3f' % effort_usdt,
		'%.3f' % plus_usdt,
		))
	fb.close()

	fa.write('[new Date({}), {}, {}],\n'.format(
		start_time,
		'%.8f' % float(sumbal),
		'%.3f' % float(sumbal*btcprice)))
	fa.close()
	return sumbal, sumbal*btcprice, balances

def get_today_balance():
	volumebook=exchange.public_get_ticker_24hr()
	allbalances=exchange.fetch_balance().get('total')
	balances={}
	for x in allbalances.keys():
		if float(allbalances.get(x))>0:
			balances[x]=float(allbalances.get(x))
	bal_list=[] #ненулевые балансы
	for b in balances.keys():
		if b!='BTC':
			lastprice=[x.get('lastPrice') for x in volumebook if x.get('symbol')==b+'BTC']
			if len(lastprice)>0:
				flprice=float(lastprice[0])*balances.get(b)
				bal_list.append(flprice)
				#print(b,'-', '%.8f' % flprice, 'BTC |', '%.3f' % (flprice*btcprice), 'USD')
			# elif len(lastprice)==0:
			# 	print(b)
		else:
			#print(b,'-', '%.8f' % float(balances.get(b)))
			bal_list.append(balances.get(b))
			#print([x.get('lastPrice') for x in volumebook if x.get('symbol')=='BTC'+b])
	return sum(bal_list), balances

def get_depos(start_time):
	f=open('results/deposits.txt', "w")
	depos=exchange.fetch_deposits(params={'startTime':start_time}) #пополнения
	dep_list=[] 
	#print(bcolors.OKGREEN,'DEPOSITS:\n====================',bcolors.ENDC)
	for dep in depos:
		# print('{cur:4} - {am:13}'.format(
		# 	cur=dep.get('currency'), 
		# 	am='%.8f' % float(dep.get('amount'))), 
		# 	end=' ')
		f.write('{cur:4} - {am:13} '.format(
			cur=dep.get('currency'), 
			am='%.8f' % float(dep.get('amount'))))
		if dep.get('currency')=='BTC':
			dep_list.append(dep.get('amount'))
			# print('{date}'.format(
			# 	date=datetime.fromtimestamp(int(dep.get('timestamp'))//1000).strftime('%d-%m-%y')))
			f.write('{date}\n'.format(
				date=datetime.fromtimestamp(int(dep.get('timestamp'))//1000).strftime('%d-%m-%y')))
			continue
		if dep.get('currency')=='USDT':
			dep_list.append(dep.get('amount')/get_usdt_price(int(dep.get('timestamp'))))
			usdt_price=get_usdt_price(int(dep.get('timestamp')))
			# print('{date} - PRICE: {price:10} USD, SUM: {sum:12} BTC'.format(
			# 	date=datetime.fromtimestamp(int(dep.get('timestamp'))//1000).strftime('%d-%m-%y'),
			# 	price='%.8f' % usdt_price,
			# 	sum=dep.get('amount')/usdt_price
			# 	))
			f.write('{date} - PRICE: {price:10} USD, SUM: {sum:12} BTC\n'.format(
				date=datetime.fromtimestamp(int(dep.get('timestamp'))//1000).strftime('%d-%m-%y'),
				price='%.8f' % usdt_price,
				sum=dep.get('amount')/usdt_price))
			continue
		# lastprice=[x.get('lastPrice') for x in volumebook if x.get('symbol')==dep.get('currency')+'BTC']
		old_price=get_history_price(int(dep.get('timestamp')),dep.get('currency')+'BTC')
		# if len(lastprice)>0:
		if old_price!=-1:
			dep_list.append(old_price*float(dep.get('amount')))
			# print('{date} - PRICE: {price:10} BTC, SUM: {sum:12} BTC'.format(
			# 	date=datetime.fromtimestamp(int(dep.get('timestamp'))//1000).strftime('%d-%m-%y'),
			# 	price='%.8f' % old_price,
			# 	sum=old_price*float(dep.get('amount'))
			# 	))
			f.write('{date} - PRICE: {price:10} BTC, SUM: {sum:12} BTC\n'.format(
				date=datetime.fromtimestamp(int(dep.get('timestamp'))//1000).strftime('%d-%m-%y'),
				price='%.8f' % old_price,
				sum=old_price*float(dep.get('amount'))))
	f.write('==================\nTOTAL:\n==================\n{} BTC'.format(sum(dep_list)))
	f.close()
	return sum(dep_list), depos


def get_withdrawals(start_time):
	f=open('results/withdrawals.txt', "w")
	wds=exchange.fetch_withdrawals(params={'startTime':start_time}) #выводы
	wd_list=[]
	#print(bcolors.OKGREEN,'WITHDRAWALS:\n====================',bcolors.ENDC)
	for wd in wds:
		# print('{cur:4} - {am:13}'.format(
		# 	cur=wd.get('currency'), 
		# 	am='%.8f' % float(wd.get('amount'))), 
		# 	end=' ')
		f.write('{cur:4} - {am:13} '.format(
			cur=wd.get('currency'), 
			am='%.8f' % float(wd.get('amount'))))
		if wd.get('currency')=='BTC':
			wd_list.append(wd.get('amount'))
			# print('{date}'.format(
			# 	date=datetime.fromtimestamp(int(wd.get('timestamp'))//1000).strftime('%d-%m-%y')))
			f.write('{date}\n'.format(
				date=datetime.fromtimestamp(int(wd.get('timestamp'))//1000).strftime('%d-%m-%y')))
			continue
		if wd.get('currency')=='USDT':
			wd_list.append(wd.get('amount')/get_usdt_price(int(wd.get('timestamp'))))
			# print('{date} - PRICE: {price:10} USD, SUM: {sum:12} BTC'.format(
			# 	date=datetime.fromtimestamp(int(wd.get('timestamp'))//1000).strftime('%d-%m-%y'),
			# 	price='%.8f' % usdt_price,
			# 	sum=wd.get('amount')/usdt_price
			# 	))
			f.write('{date} - PRICE: {price:10} USD, SUM: {sum:12} BTC\n'.format(
				date=datetime.fromtimestamp(int(wd.get('timestamp'))//1000).strftime('%d-%m-%y'),
				price='%.8f' % usdt_price,
				sum=wd.get('amount')/usdt_price))
			continue
		# lastprice=[x.get('lastPrice') for x in volumebook if x.get('symbol')==wd.get('currency')+'BTC']
		old_price=get_history_price(int(wd.get('timestamp')),wd.get('currency')+'BTC')
		if old_price!=-1:
			wd_list.append(old_price*float(wd.get('amount')))
			# print('{date} - PRICE: {price:10} BTC, SUM: {sum:12} BTC'.format(
			# 	date=datetime.fromtimestamp(int(wd.get('timestamp'))//1000).strftime('%d-%m-%y'),
			# 	price='%.8f' % old_price,
			# 	sum=old_price*float(wd.get('amount'))
			# 	))
			f.write('{date} - PRICE: {price:10} BTC, SUM: {sum:12} BTC\n'.format(
				date=datetime.fromtimestamp(int(wd.get('timestamp'))//1000).strftime('%d-%m-%y'),
				price='%.8f' % old_price,
				sum=old_price*float(wd.get('amount'))))
	f.write('==================\nTOTAL:\n==================\n{} BTC'.format(sum(wd_list)))
	f.close()
	return sum(wd_list), wds

#======================================================================================
d=get_depos(yesterday)[1]
w=get_withdrawals(yesterday)[1]
start_account_time=d[0].get('timestamp')
for x in d:
	if x.get('timestamp')<=start_account_time:
		start_account_time=x.get('timestamp')
		# if x.get('currency')=='USDT':
		# 	price=get_usdt_price(start_time)
		# 	first_day_btc=x.get('amount')/price
		# elif x.get('currency')=='BTC':
		# 	first_day_btc=x.get('amount')
		# else:
		# 	price=get_history_price(start_account_time, x.get('currency')+'BTC')
		# 	first_day_btc=x.get('amount')*price

#print('start account timestamp', start_account_time)
#======================================================================================
## ПЫЛЬ В BNB
dust=[]
dustlist=exchange.fetch_my_dust_trades()
for x in dustlist:
	mn=-1
	symbols=x.get('symbol').split('/')
	if x.get('side')=='sell':
		mn=1
	dust.append({'timestamp':int(x.get('timestamp')), 
		'amount':float((mn*-1)*x.get('amount')), 
		'cost':float(mn*x.get('cost')),
		'commission':float(x.get('fee').get('cost')),
		'av':symbols[0],
		'cv':symbols[1],
		'comv':x.get('fee').get('currency')
		})
#======================================================================================
## CДЕЛКИ НУЖНО
raw_orders={}
#markets=[x for x in list(exchange.load_markets().keys())]# if x.endswith('/BTC')]

# for x in markets:
# 	orde=exchange.fetch_my_trades(symbol=x)
# 	if len(orde)>0:
# 		raw_orders[x]=orde
# 	print('Проверяем', x)
# 	sleep(0.2)

#======================================================================================
## СДЕЛКИ ПО ФАКТУ

mk=['ETH/BTC', 'ETH/USDT', 'BQX/BTC', 'SUB/BTC',
'SUB/ETH', 'DASH/BTC', 'STORJ/BTC', 'BNB/USDT', 'BTS/BTC', 'GTO/BTC', 'STEEM/BTC',
'NCASH/BTC', 'GRS/BTC', 'IOTX/BTC', 'FET/BNB', 'FET/BTC', 'FET/USDT',
'CELR/BNB','CELR/BTC','CELR/USDT']

for x in mk:
	orde=exchange.fetch_my_trades(symbol=x)
	if len(orde)>0:
		raw_orders[x]=orde
	print('Проверяем', x)
	sleep(0.2)

#======================================================================================
## ВЫПИСКА ПО СДЕЛКАМ
#print(raw_orders)

f=open('results/trades.txt', "w")
f.write('DUST TRADES:\n======================\n'+'\n'.join([str(x) for x in dust])+'\n')
ords=[]
orders=dust
for x in raw_orders.keys():
	for y in raw_orders.get(x):
		mn=-1
		symbols=y.get('symbol').split('/')
		if y.get('side')=='sell':
			mn=1
		orders.append({'timestamp':int(y.get('timestamp')), 
			'amount':float((mn*-1)*y.get('amount')), 
			'cost':float(mn*y.get('cost')),
			'commission':float(y.get('fee').get('cost')),
			'av':symbols[0],
			'cv':symbols[1],
			'comv':y.get('fee').get('currency')
			})
		ords.append('{date} - {type}: {amount} {av} ; {cost} {cv} ; commission: {commission} {comv}'.format(
			#date=datetime.fromtimestamp(y.get('timestamp')//1000).strftime('%d-%m-%y %H:%M:%S'),
			date=y.get('timestamp'),
			type=y.get('side'),
			amount='%.8f' % float((mn*-1)*y.get('amount')),
			av=symbols[0],
			cv=symbols[1],
			comv=y.get('fee').get('currency'),
			cost='%.8f' % float(mn*y.get('cost')),
			commission='%.8f' % float(y.get('fee').get('cost'))
			))

print('===============')
f.write('ALL TRADES:\n======================\n'+'\n'.join([str(x) for x in orders]))
f.close()
#print('\n'.join(ords))
#print('\n'.join([x + ' - ' + str(len(raw_orders.get(x))) for x in raw_orders.keys() if len(raw_orders.get(x))>0]))
#print('===============')
#1561684231949 - last order
#======================================================================================
balances={}
start_date=None
btcsum=0
usdtsum=0
for offset in range(556):
	end_date=start_date or 0
	start_date=start_account_time+1+((86400000)*offset)
	print(datetime.fromtimestamp(start_date//1000).strftime('%d-%m-%y %H:%M:%S'))
	bal=get_balance_per_day(balances, start_date, end_date, d, w, orders, btcsum, usdtsum)
	btcsum=bal[0]
	usdtsum=bal[1]
	print('BALANCE:','%.8f' % bal[0], 'BTC |', '%.3f' % bal[1], 'USD')
	balances=bal[2]
	print('======================')
sleep(1000)
#======================================================================================
# for x in range(14):
# 	date=datetime.utcnow()-timedelta(hours=2+(24*x))
# 	result=get_balance_per_day(get_today_balance()[1], int(date.timestamp())*1000)
# 	print('{date} - {BTC} / {USD}'.format(
# 		date=date.strftime('%d-%m-%y %H:%M:%S'),
# 		BTC='%.8f BTC' % result[0],
# 		USD='%.3f USD' % result[1]))
#======================================================================================

# d=get_depos(yesterday)
# w=get_withdrawals(yesterday)
#b=get_today_balance()
# print('DEPOSIT:','%.8f' % d, 'BTC |', '%.3f' % (d*btcprice), 'USD')
# print('WITHDRAWALS:','%.8f' % w, 'BTC |', '%.3f' % (w*btcprice), 'USD')
#print('BALANCE:','%.8f' % b, 'BTC |', '%.3f' % (b*btcprice), 'USD')