from telebot import TeleBot, types, apihelper
import ccxt
from datetime import datetime, timedelta
from random import shuffle
from apscheduler.scheduler import Scheduler
#pip install apscheduler==2.1.2
from time import sleep
from pprint import pprint

sched = Scheduler()
sched.start()

TOKEN = 'token'
canal_link='@coin_pulse_listing'
vip=-1001309997217

#if you need run telegram bot with proxy
#apihelper.proxy = {'https':'https://195.122.185.95:3128'}

bot = TeleBot(TOKEN)
otlozheno=5

bitfinex=ccxt.bitfinex()
ethfinex=ccxt.ethfinex()
poloniex=ccxt.poloniex()
bithumb=ccxt.bithumb()
bittrex=ccxt.bittrex()
binance=ccxt.binance()
hitbtc=ccxt.hitbtc()
gateio=ccxt.gateio()
yobit=ccxt.yobit()
kraken=ccxt.kraken()
kucoin=ccxt.kucoin()
huobipro=ccxt.huobipro()
#huobiru=ccxt.huobiru()

exchanges=[
bitfinex,ethfinex,poloniex,bithumb,
bittrex,binance,hitbtc,gateio,
kraken,kucoin,huobipro
]

def getTradeLink(exchange, pair):
	if exchange is bitfinex:
		return 'https://www.bitfinex.com/trading/%s' %bitfinex.load_markets(True)[pair]['info']['pair']
	if exchange is ethfinex:
		return 'https://www.ethfinex.com/trading/%s' %ethfinex.load_markets(True)[pair]['info']['pair']
	if exchange is poloniex:
		return 'https://www.poloniex.com/exchange/%s' %poloniex.load_markets(True)[pair]['id']
	if exchange is bithumb:
		return 'https://www.bithumb.com/trade/order/%s' %bithumb.load_markets(True)[pair]['id']
	if exchange is bittrex:
		return 'https://www.bittrex.com/Market/Index?MarketName=%s' %bittrex.load_markets(True)[pair]['info']['MarketName']
	if exchange is binance:
		return 'https://www.binance.com/trade.html?symbol=%s' %binance.load_markets(True)[pair]['symbol'].replace('/','_')
	if exchange is hitbtc:
		return 'https://hitbtc.com/exchange/%s' %pair.replace('/','-to-')
	if exchange is gateio:
		return 'https://www.gate.io/trade/%s' %gateio.load_markets(True)[pair]['symbol'].replace('/','_')
	if exchange is yobit:
		return 'https://yobit.net/ru/trade/%s' %pair
	if exchange is kraken:
		return 'https://trade.kraken.com/markets/kraken/%s' %pair
	if exchange is kucoin:
		return 'https://www.kucoin.com/#/trade.pro/%s' %kucoin.load_markets(True)[pair]['info']['symbol']
	if exchange is huobipro:
		return 'https://www.hbg.com/en-us/exchange/%s/' %huobipro.load_markets(True)[pair]['symbol'].replace('/','_').lower()

def deffered_message(message):
	global vip
	bot.send_message(chat_id=vip, text=message, parse_mode="Markdown", disable_web_page_preview=True)

pprint(bot.send_message(chat_id=vip, text='hi', parse_mode="Markdown", disable_web_page_preview=True))
sleep(100)
# Start script
for ex in exchanges:
	f=open('/home/alex/ftp/files/coinlisting/%s.txt' %str(ex), 'w')
	keys=ex.load_markets(True)
	f.writelines('%s\n' %key for key in keys)
	f.close()



def check_exchange():
	global canal_link
	while(True):
		shuffle(exchanges)
		for ex in exchanges:
			try:
				f=open('/home/alex/ftp/files/coinlisting/%s.txt' %str(ex))
				keys=ex.load_markets(True)
				lines=list(map(lambda x: x.replace('\n',''), f.readlines()))
				f.close()
				forwrite=[]
				for key in keys.keys():
					if key not in lines:
						bot.send_message(
							chat_id=canal_link, 
							text='*{}* listed on [{}]({}):\n([{}]({}))'.format(keys[key]['base'], str(ex), ex.describe()['urls']['www'], key, getTradeLink(ex,key)), 
							parse_mode="Markdown",
							disable_web_page_preview=True)
						forwrite.append(key)
						now = datetime.now()
						run_at = now + timedelta(seconds=otlozheno*60)
						sched.add_date_job(deffered_message, run_at, ['*{}* listed on [{}]({}):\n([{}]({}))'.format(keys[key]['base'], str(ex), ex.describe()['urls']['www'], key, getTradeLink(ex,key))])
				print('{} - exchange {} has been checked'.format(datetime.now(), str(ex)))
				f=open('/home/alex/ftp/files/coinlisting/%s.txt' %str(ex), 'a')
				f.writelines('%s\n' %key for key in forwrite)
			except Exception as e:
				print(e)

if __name__ == '__main__':
	check_exchange()